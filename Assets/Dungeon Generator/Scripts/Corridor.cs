﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Corridor : MonoBehaviour
{
    
    bool hasCorner = false;
    public bool tileSize3 = false;

    public Tile floorTile;
    public Tile wallTile;

    public Tilemap floor;
    public Tilemap wall;

    [SerializeField] GameObject cornerGo;
    [SerializeField] GameObject endGo;

    Vector3Int startPos = Vector3Int.zero;
    Vector3Int cornerPos = Vector3Int.zero;
    Vector3Int endPos = Vector3Int.zero;

    Grid grid;
    

    // Start is called before the first frame update
    void Start()
    {
        //InitCorridor();
    }

    public void InitCorridor()
    {
        grid = this.GetComponentInParent<Grid>();
        startPos = grid.WorldToCell(this.transform.position);
        endPos = grid.WorldToCell(endGo.transform.position);

        this.transform.position = grid.CellToWorld(startPos);
        endGo.transform.position = grid.CellToWorld(endPos);

        if (startPos.x != endPos.x && startPos.y != endPos.y)
        {
            hasCorner = true;

            Vector3 temp = Vector3.zero;

            if (Mathf.Abs(endPos.x - startPos.x) > Mathf.Abs(endPos.y - startPos.y))
            {
                temp.x = endGo.transform.position.x;
                temp.y = startPos.y;
            }
            else
            {
                temp.x = startPos.x;
                temp.y = endGo.transform.position.y;
            }

            
            cornerPos = grid.WorldToCell(temp);
            cornerGo.transform.position = cornerPos;
        }

        if (hasCorner)
        {
           DrawCorridor(startPos, cornerPos);
           DrawCorridor(cornerPos, endPos);
        }
        else
        {
            DrawCorridor(startPos, endPos);
        }

        DrawWalls();
    }

    public void SetEndPosition(Vector3 pos)
    {
        endGo.transform.position = pos;
    }

    void DrawCorridor(Vector3Int start, Vector3Int end)
    {
        if(start.x != end.x)
        {
            if(end.x > start.x)
            {
                int x = 0;
                for (int i = start.x; i < end.x; i++)
                {
                    Vector3Int pos = new Vector3Int(x, start.y - (int)this.transform.position.y, start.z);

                    if (!tileSize3)
                    {
                        floor.SetTile(pos, floorTile);
                        floor.SetTile(pos + Vector3Int.up, floorTile);
                    }
                    else
                    {
                        floor.SetTile(pos, floorTile);
                        floor.SetTile(pos + Vector3Int.up, floorTile);
                        floor.SetTile(pos + Vector3Int.down, floorTile);
                    }

                    x++;
                    
                }
            }
            else
            {
                int x = 0;
                for (int i = start.x; i > end.x; i--)
                {
                    Vector3Int pos = new Vector3Int(x, start.y - (int)this.transform.position.y, start.z);

                    if (!tileSize3)
                    {
                        floor.SetTile(pos, floorTile);
                        floor.SetTile(pos + Vector3Int.up, floorTile);
                    }
                    else
                    {
                        floor.SetTile(pos, floorTile);
                        floor.SetTile(pos + Vector3Int.up, floorTile);
                        floor.SetTile(pos + Vector3Int.down, floorTile);
                    }

                    x--;
                    
                }
            }
        }
        else if (start.y != end.y)
        {
            if (end.y > start.y)
            {
                int y = 0;
                for (int i = start.y; i < end.y; i++)
                {
                    Vector3Int pos = new Vector3Int(start.x - (int)this.transform.position.x, y, start.z);

                    if (!tileSize3)
                    {
                        floor.SetTile(pos, floorTile);
                        floor.SetTile(pos + Vector3Int.right, floorTile);
                    }
                    else
                    {
                        floor.SetTile(pos, floorTile);
                        floor.SetTile(pos + Vector3Int.right, floorTile);
                        floor.SetTile(pos + Vector3Int.left, floorTile);
                    }

                    y++;
                }
            }
            else
            {
                int y = 0;
                for (int i = start.y; i > end.y; i--)
                {
                    Vector3Int pos = new Vector3Int(start.x - (int)this.transform.position.x, y, start.z);

                    if (!tileSize3)
                    {
                        floor.SetTile(pos, floorTile);
                        floor.SetTile(pos + Vector3Int.right, floorTile);
                    }
                    else
                    {
                        floor.SetTile(pos, floorTile);
                        floor.SetTile(pos + Vector3Int.right, floorTile);
                        floor.SetTile(pos + Vector3Int.left, floorTile);
                    }

                    y--;
                }
            }
        }
    }

    void DrawWalls()
    {
        BoundsInt boundsInt = floor.cellBounds;

        for (int x = boundsInt.xMin; x < boundsInt.xMax; x++)
        {
            for (int y = boundsInt.yMin; y < boundsInt.yMax; y++)
            {
                Vector3Int pos = new Vector3Int(x, y, 0);
                if(floor.GetTile(pos) != null)
                {
                    if(floor.GetTile(pos + Vector3Int.up) == null)
                    {
                        wall.SetTile(pos + Vector3Int.up, wallTile);
                    }

                    if (floor.GetTile(pos + Vector3Int.right) == null)
                    {
                        wall.SetTile(pos + Vector3Int.right, wallTile);
                    }

                    if (floor.GetTile(pos + Vector3Int.down) == null)
                    {
                        wall.SetTile(pos + Vector3Int.down, wallTile);
                    }

                    if (floor.GetTile(pos + Vector3Int.left) == null)
                    {
                        wall.SetTile(pos + Vector3Int.left, wallTile);
                    }
                }
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(this.transform.position, .5f);

        if (hasCorner)
        {
            Gizmos.DrawLine(this.transform.position, cornerGo.transform.position);

            Gizmos.color = Color.cyan;
            Gizmos.DrawSphere(cornerGo.transform.position, .5f);

            Gizmos.DrawLine(cornerGo.transform.position, endGo.transform.position);
        }
        else
        {
            Gizmos.DrawLine(this.transform.position, endGo.transform.position);
        }
        

        Gizmos.color = Color.red;
        Gizmos.DrawSphere(endGo.transform.position, .5f);
    }
}
