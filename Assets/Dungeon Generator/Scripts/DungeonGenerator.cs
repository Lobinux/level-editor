﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.Tilemaps;

public class DungeonGenerator : MonoBehaviour
{
    [SerializeField] GameObject roomPrefab;
    [SerializeField] GameObject corridorPref;

    public int gridDimensions = 32;

    int minRoomInScene = 1;
    public int roomsInScnene = 2;

    [SerializeField] List<Room> rooms = new List<Room>();
    List<Corridor> corridors = new List<Corridor>();
    List<Region> regions = new List<Region>();
    List<RegionsPair> conections = new List<RegionsPair>();

    List<Tilemap> originalFloorMap = new List<Tilemap>();
    List<Tilemap> originalWallMap = new List<Tilemap>();

    List<TilemapData> floormapsData = new List<TilemapData>();
    List<TilemapData> wallmapsData = new List<TilemapData>();

    public Tilemap targetFloormap;
    public bool floorUseCollider = false;
    public Tilemap targetWallmap;
    public bool wallUseCollider = false;

    void Start()
    {
        Random.InitState(System.DateTime.Now.Second);

        Init();
    }

    void Init()
    {
        if (!targetFloormap)
        {
            targetFloormap = CreateTilemap("Target Floor Map", 0, floorUseCollider);
        }

        if (!targetWallmap)
        {
            targetWallmap = CreateTilemap("Target Wall Map", 1, wallUseCollider);
        }

        if (roomsInScnene < minRoomInScene)
        {
            roomsInScnene = minRoomInScene;
        }

        for (int i = 0; i < roomsInScnene; i++)
        {
            if (rooms.Count < roomsInScnene)
            {
                
                rooms.Add(InstantiateRooms(roomPrefab, Mathf.RoundToInt(Time.realtimeSinceStartup * Mathf.PI + i)));
            }
        }

        CreateRegions();

        CheckDistanceForConections();

        CreateCorridorsFromConections();
        
        GetDataFromRoomsAndCorridors();
        
        CombineDataInTilamap(floormapsData, targetFloormap);
        
        CombineDataInTilamap(wallmapsData, targetWallmap);

        CleanUnecesaryWalls();
        
    }

    Room InstantiateRooms(GameObject roomPref, int seed = 0)
    {
        Room returnRoom;
        Vector3 pos = Vector3.zero;
        pos.x = Random.Range(-gridDimensions / 2, gridDimensions / 2);
        pos.y = Random.Range(-gridDimensions / 2, gridDimensions / 2);

        GameObject go = Instantiate(roomPref, pos, Quaternion.identity, this.transform);

        returnRoom = go.GetComponent<Room>();

        returnRoom.InitRoom(new Vector2Int( Random.Range(4, 8), Random.Range(4, 8)));

        return returnRoom;
    }

    void CreateRegions()
    {
        if (regions.Count <= 0)
        {
            Region region = new Region();
            region.rRooms.Add(rooms[0]);
            region.conectedPoint = rooms[0];
            regions.Add(region);
        }

        for (int i = 1; i < rooms.Count; i++)
        {
            bool result = false;
            Region selectedRegion = null;

            for (int j = 0; j < regions.Count; j++)
            {
                if (RoomCollidesWithRegion(rooms[i], regions[j])) {
                    result = true;
                    selectedRegion = regions[j];
                    break;
                }
            }

            if (result)
            {
                selectedRegion.rRooms.Add(rooms[i]);
            }
            else
            {
                Region newRegion = new Region();
                newRegion.rRooms.Add(rooms[i]);
                newRegion.conectedPoint = rooms[i];
                regions.Add(newRegion);
            }
        }
    }

    bool RoomCollidesWithRegion(Room room, Region region)
    {
        bool result = false;

        for (int k = 0; k < region.rRooms.Count; k++)
        {
            if (RoomsOverlaping(room, region.rRooms[k]))
            {
                result = true;
                break;
            }
        }
        
        return result;
    }

    void CheckDistanceForConections()
    {
        if(regions.Count > 1)
        {
            for (int i = 0; i < regions.Count; i++)
            {
                RegionsPair pair = new RegionsPair();
                pair = CheckRegionPairs(regions[i], regions);

                conections.Add(pair);
            }
        }
    }

    void CreateCorridorsFromConections()
    {
        if(conections.Count > 0)
        {
            for (int i = 0; i < conections.Count; i++)
            {
                if(conections[i].regionA != null)
                {
                    if (conections[i].regionA.conectedPoint != null)
                    {
                        corridors.Add(InstantiateCorridors(corridorPref, conections[i].regionA));
                    }
                }
            }
        }
    }

    RegionsPair CheckRegionPairs(Region A, List<Region> regions)
    {
        RegionsPair shortest = new RegionsPair();
        shortest.dist = gridDimensions;

        for (int i = 0; i < regions.Count; i++)
        {
            if(A != regions[i] && regions[i].conectedTo == null)
            {
                float evalDist = Vector3.Distance(A.conectedPoint.transform.position, regions[i].conectedPoint.transform.position);

                if (evalDist < shortest.dist)
                {
                    shortest.regionA = A;
                    shortest.regionB = regions[i];
                    shortest.dist = evalDist;
                    A.conectedTo = regions[i].conectedPoint;
                }
            }
        }

        return shortest;
    }

    Corridor InstantiateCorridors(GameObject corridorPref, Region region)
    {
        Corridor returnCorridor;
        GameObject go = Instantiate(corridorPref, region.conectedPoint.transform.position, Quaternion.identity, this.transform);

        returnCorridor = go.GetComponent<Corridor>();

        returnCorridor.SetEndPosition(region.conectedTo.transform.position);

        returnCorridor.InitCorridor();

        return returnCorridor;
    }

    void GetDataFromRoomsAndCorridors()
    {

        foreach (var corridor in corridors)
        {
            originalFloorMap.Add(corridor.floor);
            originalWallMap.Add(corridor.wall);
        }

        foreach (var room in rooms)
        {
            originalFloorMap.Add(room.floor);
            originalWallMap.Add(room.wall);
        }

        if(targetFloormap != null)
        {
            floormapsData = GetDataFromMaps(originalFloorMap);
        }

        if (targetWallmap != null)
        {
            wallmapsData = GetDataFromMaps(originalWallMap);
        }
    }

    bool RoomsOverlaping(Room A, Room B)
    {
        bool retVal = false;

        if (A.roomBounds.Intersects(B.roomBounds) || B.roomBounds.Intersects(A.roomBounds))
        {
            retVal = true;
        }

        return retVal;
    }


    List<TilemapData> GetDataFromMaps(List<Tilemap> tilemaps)
    {
        List<TilemapData> tilemapDatas = new List<TilemapData>();

        if (tilemaps.Count > 0)
        {
            BoundsInt[] cellBounds = new BoundsInt[tilemaps.Count];

            for (int i = 0; i < tilemaps.Count; i++)
            {
                cellBounds[i] = tilemaps[i].cellBounds;
                Vector3Int originPos = Vector3Int.FloorToInt(tilemaps[i].transform.position);
                TilemapData tilemapData = new TilemapData();

                for (int x = cellBounds[i].xMin; x < cellBounds[i].xMax; x++)
                {
                    for (int y = cellBounds[i].yMin; y < cellBounds[i].yMax; y++)
                    {
                        TileData tileData = new TileData();
                        Vector3Int logicPos = new Vector3Int(x, y, 0);

                        if (tilemaps[i].GetTile(logicPos) != null)
                        {
                            tileData.tile = tilemaps[i].GetTile(logicPos);
                            tileData.logicPosition = logicPos + originPos;
                            tilemapData.tilesData.Add(tileData);
                        }
                    }
                }

                tilemapDatas.Add(tilemapData);
                tilemaps[i].gameObject.SetActive(false);
            }
        }
        return tilemapDatas;
    }


    void CombineDataInTilamap(List<TilemapData> tilemapDatas, Tilemap tilemap)
    {
        if(tilemapDatas.Count > 0)
        {
            foreach (TilemapData tilemapData in tilemapDatas)
            {
                for (int i = 0; i < tilemapData.tilesData.Count; i++)
                {
                   tilemap.SetTile(tilemapData.tilesData[i].logicPosition, tilemapData.tilesData[i].tile);
                }
            }
        }
    }

    void CleanUnecesaryWalls()
    {
        BoundsInt cellBounds = targetWallmap.cellBounds;

        for (int y = cellBounds.yMin + 1; y < cellBounds.xMax + 1; y++)
        {
            for (int x = cellBounds.xMin + 1; x < cellBounds.xMax + 1; x++)
            {
                Vector3Int coord = new Vector3Int(x, y, 0);
                if (targetFloormap.GetTile(coord) != null)
                {
                    targetWallmap.SetTile(coord, null);
                }
            }
        }
    }

    Tilemap CreateTilemap(string name, int sortOrder, bool useCollide)
    {
        GameObject go = new GameObject(name, typeof(Tilemap), typeof(TilemapRenderer));

        if (useCollide)
        {
            go.AddComponent<TilemapCollider2D>();
            TilemapCollider2D tilemapCollider2D = go.GetComponent<TilemapCollider2D>();
            tilemapCollider2D.usedByComposite = true;

            go.AddComponent<CompositeCollider2D>();
            CompositeCollider2D compositeCollider2D = go.GetComponent<CompositeCollider2D>();
            compositeCollider2D.geometryType = CompositeCollider2D.GeometryType.Polygons;

            go.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        }

        go.transform.SetParent(this.transform);
        Tilemap tilemap = go.GetComponent<Tilemap>();
        tilemap.GetComponent<TilemapRenderer>().sortingOrder = sortOrder;

        return tilemap;
    }

    private void OnDrawGizmos()
    {
        if(conections.Count > 0)
        {
            foreach (var item in conections)
            {
                if(item.regionA != null)
                {
                    if (item.regionA.conectedPoint != null)
                    {
                        Gizmos.color = Color.red;
                        Gizmos.DrawSphere(item.regionA.conectedPoint.transform.position, .5f);
                        Gizmos.DrawSphere(item.regionB.conectedPoint.transform.position, .5f);
                        Gizmos.DrawLine(item.regionA.conectedPoint.transform.position, item.regionB.conectedPoint.transform.position);
                    }
                }
                
            }
        }
    }

    public Tilemap GetWallkableTilemap()
    {
        return targetFloormap;
    }
}

[System.Serializable]
public class TilemapData
{
    public List<TileData> tilesData = new List<TileData>();
}

[System.Serializable]
public class TileData
{
    public TileBase tile = null;
    public Vector3Int logicPosition = Vector3Int.zero;
}

[System.Serializable]
public class Region
{
    public Room conectedPoint = null;
    public Room conectedTo = null;
    public List<Room> rRooms = new List<Room>();
}

[System.Serializable]
public class RegionsPair
{
    public Region regionA = null;
    public Region regionB = null;
    public float dist = 0;
}
