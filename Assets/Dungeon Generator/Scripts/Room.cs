﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Room : MonoBehaviour
{
    public bool isRoomInitialized = false;
    public Tile floorTile;
    public Tile wallTile;

    public Bounds roomBounds;
    public Vector3Int logicPos;

    public Tilemap floor;
    public Tilemap wall;

    [SerializeField] Vector2Int roomSize;

    private void Start()
    {
        //InitRoom();
    }

    public void InitRoom()
    {
        if (isRoomInitialized == true)
            return;

        ClearTilemapRoom(floor);
        ClearTilemapRoom(wall);

        CreateRoom(roomSize);

        isRoomInitialized = true;
    }

    public void InitRoom(Vector2Int roomSize)
    {
        if (isRoomInitialized == true)
            return;

        ClearTilemapRoom(floor);
        ClearTilemapRoom(wall);

        CreateRoom(roomSize);

        isRoomInitialized = true;
    }

    void CreateRoom(Vector2Int roomSize)
    {
        logicPos = Vector3Int.CeilToInt(this.transform.position);
        this.transform.position = logicPos;
        this.roomSize = roomSize;

        Vector3Int minPosition = new Vector3Int(logicPos.x - (roomSize.x / 2), logicPos.y - (roomSize.y / 2), logicPos.z);
        Vector3Int maxPosition = new Vector3Int(logicPos.x + (roomSize.x / 2), logicPos.y + (roomSize.y / 2), logicPos.z);

        roomBounds.SetMinMax(minPosition, maxPosition);

        CreateFloor(roomSize);

        CreateWall(roomSize);
    }

    void CreateFloor(Vector2Int roomSize) {

        Vector3Int roomBounds = new Vector3Int((roomSize.x / 2) - roomSize.x, (roomSize.y / 2) - roomSize.y, 0);

        //Fill floor
        for (int y = roomBounds.y; y < roomSize.y / 2; y++)
        {
            for (int x = roomBounds.x; x < roomSize.x / 2; x++)
            {
                Vector3Int coord = new Vector3Int(x, y, 0);
                floor.SetTile(coord, floorTile);
            }
        }
    }

    void CreateWall(Vector2Int roomSize)
    {
        Vector3Int roomBounds = new Vector3Int((roomSize.x / 2) - roomSize.x, (roomSize.y / 2) - roomSize.y, 0);

        //Fill Wall
        for (int y = roomBounds.y; y < roomSize.y / 2; y++)
        {
            for (int x = roomBounds.x; x < roomSize.x / 2; x++)
            {
                Vector3Int coord = new Vector3Int(x, y, 0);
                if (floor.GetTile(coord) == null)
                {
                    wall.SetTile(coord, wallTile);
                }
                else
                {
                    //Fill walls in 8 directions of the tile
                    if (floor.GetTile(coord + Vector3Int.up) == null)
                    {
                        wall.SetTile(coord + Vector3Int.up, wallTile);
                    }

                    if (floor.GetTile(coord + (Vector3Int.up + Vector3Int.right)) == null)
                    {
                        wall.SetTile(coord + (Vector3Int.up + Vector3Int.right), wallTile);
                    }

                    if (floor.GetTile(coord + Vector3Int.right) == null)
                    {
                        wall.SetTile(coord + Vector3Int.right, wallTile);
                    }

                    if (floor.GetTile(coord + (Vector3Int.down + Vector3Int.right)) == null)
                    {
                        wall.SetTile(coord + (Vector3Int.down + Vector3Int.right), wallTile);
                    }

                    if (floor.GetTile(coord + (Vector3Int.down + Vector3Int.left)) == null)
                    {
                        wall.SetTile(coord + (Vector3Int.down + Vector3Int.left), wallTile);
                    }

                    if (floor.GetTile(coord + Vector3Int.left) == null)
                    {
                        wall.SetTile(coord + Vector3Int.left, wallTile);
                    }

                    if (floor.GetTile(coord + (Vector3Int.up + Vector3Int.left)) == null)
                    {
                        wall.SetTile(coord + (Vector3Int.up + Vector3Int.left), wallTile);
                    }
                }
            }
        }
    }

    void ClearTilemapRoom(Tilemap tilemap)
    {
        BoundsInt boundsInt = tilemap.cellBounds;

        for (int y = boundsInt.yMin; y < boundsInt.yMax; y++)
        {
            for (int x = boundsInt.xMin; x < boundsInt.xMax; x++)
            {
                tilemap.SetTile(new Vector3Int(x, y, 0), null);
            }
        }
        tilemap.origin = Vector3Int.zero;
        tilemap.size = new Vector3Int(0, 0, 1);
    }

    public void ResetRoom()
    {
        ClearTilemapRoom(floor);
        ClearTilemapRoom(wall);
    }
}
